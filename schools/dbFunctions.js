const { firestore } = require("../connections/firestore");

const getSchoolData = async({schoolId}) =>{
    console.log("getting schoolId ",schoolId," data")
    try{
        let fetchSchool = await firestore.collection('schools').doc(schoolId).get();
        let schoolData = fetchSchool?.data() || false
        if (schoolData) {
            schoolData = {...schoolData, schoolId: schoolId}
        }
        return schoolData
    }catch(err){
        console.log("error in getting school doc from firestore", err)
        return false
    }
}

module.exports = {getSchoolData: getSchoolData}

/* const getSchoolData = async({shortUrl}) =>{
    console.log("getting url data")
    try{
        let querySnapshot = await firestore.collection('short_urls').where("shortUrl", "==", shortUrl).orderBy('createdAt').limit(1).get();
        let urlData = querySnapshot?.docs?.[0]?.data() || false
        if (urlData) {
            urlData = {...urlData, docId: querySnapshot?.docs?.[0]?.id}
        }
        return urlData
    }catch(err){
        console.log("error in getting url docs from firestore", err)
        return false
    }
}
 */