//this handler may be broadned in great ways in the future and might even becomea general helper component
//for now it serves only the admission tasks and compares within the same doc

//condition stracture:
//{
//    type: [mandatoryfieldsExists || compare],
//    failOutput: ["inProgress" || "rejected"],
//    successOutput: ["passed"]
//    compareObject {compareType, value, docField}
//}
const conditionsHandler = async({condition, doc, mandatoryFields}) =>{
    const failOutput = condition?.failOutput || false
    const successOutput = condition?.successOutput || true

    if (condition.type === "mandatoryfieldsExists"){
        for (let fieldKey of mandatoryFields){
            if (!doc?.[fieldKey]){
                return failOutput
            }
        }
        return successOutput
    }

    if (condition.type === "compare"){
        let compareType = condition?.compareObject["compareType"]
        let docField = condition?.compareObject["docField"]
        let value = condition?.compareObject["value"]
        if (compareType && docField in doc){
            if (compareType === ">"){
                return (doc[docField] > doc[value])
                    ?   successOutput
                    :   failOutput
            }
            if (compareType === "=="){
                return (doc[docField] === doc[value])
                    ?   successOutput
                    :   failOutput
            }
            if (compareType === "<"){
                return (doc[docField] < doc[value])
                    ?   successOutput
                    :   failOutput
            }
        }
        return failOutput
    }
}


module.exports = {conditionsHandler: conditionsHandler}