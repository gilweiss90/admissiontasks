const { firestore } = require("../connections/firestore");


const getAdmissionTaskTemplateData = async({taskTemplateId}) =>{
    console.log("getting admission task template: ",TaskTemplateId, " data")
    try{
        let fetchTaskTemplate = await firestore.collection('admission_task_templates').doc(TaskTemplateId).get();
        let taskTemplate = fetchTaskTemplate?.data() || false
        if (taskTemplate) {
            taskTemplate = {...taskTemplate, taskTemplateId: taskTemplateId}
        }
        return taskTemplate || false
    }catch(err){
        console.log("error in getting admission task template doc from firestore", err)
        return false
    }
}


const getUserAdmissionTaskData = async({TaskTemplateId, userId}) =>{
    console.log("getting user admission task: ",TaskTemplateId, " data")
    try{
        let userAdmissionTaskQuerySnapshot = await firestore.collection('users_admission_tasks').where("userId", "==", userId).where("admissionTaskTemplateId", "==", TaskTemplateId).get();
        let userAdmissionTask = userAdmissionTaskQuerySnapshot?.docs?.[0]?.data() || false
        if (userAdmissionTask) {
            userAdmissionTask = {...userAdmissionTask, userAdmissionTaskId: userAdmissionTaskQuerySnapshot?.docs?.[0]?.id}
        }
        return userAdmissionTask || false
    }catch(err){
        console.log("error in getting user admission task doc from firestore", err)
        return false
    }
}


module.exports = {
    getAdmissionTaskTemplateData: getAdmissionTaskTemplateData,
    getUserAdmissionTaskData: getUserAdmissionTaskData
}