const { conditionsHandler } =require("./conditionsHandler")


const getUserAdmissionTasksSummary = async({userId, schoolId}) =>{
    const { getSchoolData } = require("../schools/dbFunctions")
    let school = await getSchoolData({schoolId: schoolId})
    if (school?.tasks){
        let taskTemplates = school.taskTemplates
        let userTasksSummaryArray = []
        for (let taskTemplate of taskTemplates){
            userTasksSummaryArray.push(getUserAdmissionTaskStatus({taskTemplateId: taskTemplate.id, userId}))
        }
        await Promise.allSettled(userTasksSummaryArray)
        let userAdmissionStatus = "InProgress"
        let userCurrentStepName = ""
        let userCurrentTaskName = ""
        let userCurrentStepIndex = 1
        let TotalNumberOfSteps = userTasksSummaryArray.length
        for (let taskIndex = 1; taskIndex <= userTasksSummaryArray.length; taskIndex++){
            if (userTasksSummaryArray[taskIndex]){
                userCurrentStepIndex = taskIndex
                userAdmissionStatus = userTasksSummaryArray[taskIndex].taskStatus
                userCurrentStepName = userTasksSummaryArray[taskIndex].stepName
                userCurrentTaskName = userTasksSummaryArray[taskIndex].taskName
                if (userTasksSummaryArray[taskIndex].taskStatus !== "passed") {
                    break;
                }
            } else {
                console.error("couldnt fetch a user admission task from school", schoolId, " and user: ", userId," it might not have been created yet")
                if (userAdmissionStatus === "passed"){
                    userAdmissionStatus = "inProgress"
                }
                break;
            }
        }
        return ({userAdmissionStatus, userCurrentStepName, userCurrentTaskName, TotalNumberOfSteps, userCurrentStepIndex, userTasksSummaryArray})
    }
    console.error("couldnt fetch school", schoolId)
    return false
}





const getUserAdmissionTaskStatus = async({taskTemplateId, userId}) =>{
    const { getAdmissionTaskTemplateData, getUserAdmissionTaskData } = require("./dbFunctions")
    let admissionTaskTemplateData = await getAdmissionTaskTemplateData({taskTemplateId})
    if (admissionTaskTemplateData){
        let taskStatus = "InProgress"
        let stepName = admissionTaskTemplateData.stepName
        let currentTaskName =  admissionTaskTemplateData.stepName
        let userAdmissionTaskData = await getUserAdmissionTaskData({taskTemplateId, userId})
        //task status
        if (userAdmissionTaskData){
            let mandatoryFields = Object.keys(admissionTaskTemplateData)
            mandatoryFields.filter((fieldKey) => {return (
                fieldKey !== "passConditions" && fieldKey !== "subTasks" && fieldKey !== "userAdmissionTaskId"
            )})
            for (let condition of admissionTaskTemplateData.passConditions){
                taskStatus = await conditionsHandler({condition, doc: userAdmissionTaskData, mandatoryFields})
                if (taskStatus !== "passed") {
                    break;
                }
            }
            ///sub tasks status
            if (taskStatus === "passed" && admissionTaskTemplateData.subTasks){
                let userSubTasksSummaryArray = []
                for (let subTaskTemplate of admissionTaskTemplateData.subTasks){
                    let pushSubtask = true
                    if (subTaskTemplate.conditions){ //subtask will appear if condition is fullfilled
                        let conditionsArray = []
                        for (let condition of subTaskTemplate.conditions){
                            conditionsArray.push (conditionsHandler({condition, doc: userAdmissionTaskData}))
                            await Promise.allSettled(userSubTasksSummaryArray)
                        }
                        pushSubtask = !conditionsArray.includes(false)
                    }
                    if (pushSubtask){
                        userSubTasksSummaryArray.push(getUserAdmissionTaskStatus({taskTemplateId: subTaskTemplate.Id, userId}))
                    }
                }
                await Promise.allSettled(userSubTasksSummaryArray)
                for (let subTaskSummary of userSubTasksSummaryArray){
                    if (subTaskSummary){
                        currentTaskName = subTaskSummary.userCurrentTaskName
                        taskStatus = subTaskSummary.taskStatus
                        if (subTaskSummary.taskStatus !== "passed") {
                            break;
                        }
                    }else {
                        console.error("couldnt fetch a user sub task from template:", taskTemplateId, " of user: ", userId," it might not have been created yet")
                        if (taskStatus === "passed"){
                            taskStatus = "inProgress"
                        }
                        break;
                    }
                }
            }
        }
        return ({taskStatus, stepName, currentTaskName})
    }
    console.error("couldnt fetch a task tamplate:", taskTemplateId)
    return (false)
}


module.exports = {getUserAdmissionTasksSummary: getUserAdmissionTasksSummary}