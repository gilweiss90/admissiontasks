const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('dist'));


app.get(
    '/api/getSchoolAdmissionTasksData',
    async (request, response) => {
        let schoolId = request?.params?.schoolId;
        let userId = request?.params?.userId;
        const { getUserAdmissionTasksSummary } = require("./admissionTasks/getUserAdmissionTasksSummary")
        let admissionTasksSummary = await getUserAdmissionTasksSummary({schoolId, userId}).catch(e => { console.log(e) })
        if (admissionTasksSummary){
            response.send({...admissionTasksSummary});
        } else ( response.send(false))
    });    
    

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));


