const { initializeApp, cert } = require('firebase-admin/app');
const { getFirestore, FieldValue} = require('firebase-admin/firestore');
const firestoreCreds = require('../../secrets/firestoreConfig.json');

initializeApp({
  credential: cert(firestoreCreds)
});

const db = getFirestore();

module.exports = { firestore: db, FieldValue: FieldValue };
